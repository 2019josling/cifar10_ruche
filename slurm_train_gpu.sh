#!/bin/bash
#SBATCH --job-name=cifar10_train_gpu
#SBATCH --output=%x.o%j
#SBATCH --time=01:00:00
#SBATCH --ntasks=24
#SBATCH --gres=gpu:2
#SBATCH --partition=gpu

# Module load
module load anaconda3/2020.02/gcc-9.2.0
module load cuda/10.2.89/intel-19.0.3.199

[ ! -d output ] && mkdir output

# Activate anaconda environment code
source activate cifar10

# Train the network
python scripts/train_network_gpu.py -n 2 # -n option set the number of GPUs (default 0, run on CPU only)
