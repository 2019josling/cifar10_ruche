module load anaconda3/2020.02/gcc-9.2.0
module load cuda/10.2.89/intel-19.0.3.199
conda create --name cifar10
source activate cifar10
conda install pytorch torchvision -c pytorch
conda install matplotlib -c pytorch
conda env export > config/environment.yml # save conda environment description
